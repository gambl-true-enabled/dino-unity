﻿using UnityEngine;

public class MamonthJump : MonoBehaviour
{
    public GameObject mamonth;
    public float jumpForce = 450f;


    private void OnMouseDown()
    {
        if (MamonthPosition.lose)
            return;
        if (mamonth.transform.position.y <= -2.87f)
        {
            mamonth.GetComponent<Rigidbody2D>().AddForce(Vector3.up * jumpForce);
            mamonth.GetComponent<Animator>().SetTrigger("MamonthJump");
        }
    }
}
