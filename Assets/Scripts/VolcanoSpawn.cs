﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class VolcanoSpawn : MonoBehaviour
{
    public GameObject volcano;
    public Text score;
    public static Text scoreText;
    public static int destroyedScore = 0;

    void Start()
    {
        destroyedScore = 0;
        scoreText = score;
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        while (!MamonthPosition.lose)
        {
            Instantiate(
                volcano,
                new Vector2(10.7f, Random.Range(-2.35f, -2.6f)),
                Quaternion.identity
            );
            yield return new WaitForSeconds(Random.Range(2, 6));
        }
        scoreText = null;
    }
}
