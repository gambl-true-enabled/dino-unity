﻿using UnityEngine;
using UnityEngine.UI;

public class VolcanoRun : MonoBehaviour
{
    private float goLeftSpeed = 6f;

    void Update()
    {
        if (transform.position.x <= -10f)
        {
            Destroy(gameObject);
            VolcanoSpawn.destroyedScore++;
            if (VolcanoSpawn.scoreText != null && !MamonthPosition.lose)
                VolcanoSpawn.scoreText.text = "Score: " + VolcanoSpawn.destroyedScore.ToString();
        }
        //if (Player.lose)
        //    transform.position -= new Vector3(0, fallSpeed * Time.deltaTime, 0);
        //else
            transform.position -= new Vector3(goLeftSpeed * Time.deltaTime, 0, 0);
    }
}
