﻿using UnityEngine;

public class MamonthPosition : MonoBehaviour
{
    public static bool lose = false;
    public GameObject explosion;
    public GameObject retry;

    private void Awake()
    {
        lose = false;
    }

    void Update()
    {
        if (lose)
            return;
        if (transform.position.x != -6f)
            transform.position = new Vector2(-6f, transform.position.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Volcano")
        {
            lose = true;
            retry.SetActive(true);
            Instantiate(
                explosion,
                transform.position,
                Quaternion.identity
            );
            Destroy(gameObject);
            //retry.SetActive(true);
            //Instantiate(
            //    explosion,
            //    transform.position,
            //    Quaternion.identity
            //);
        }
    }
}
